Homepage
========
Studying Scala2.10.x and Wicket6.x.

## Environments
* Scala 2.10.2
* Wicket 6.8.0
* Maven 3.2.1

## See also
* http://blog.rubezhanskyy.com/2013/wicket-scala-quickstart/
