package jp.cignoir

import org.apache.wicket.markup.html.WebPage
import org.apache.wicket.request.mapper.parameter.PageParameters
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.model.ResourceModel

class MyPage extends WebPage{
  def helloLabel = new Label("helloWorldLabel", "hoge")
  add(helloLabel)
}